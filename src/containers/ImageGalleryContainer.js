import { connect } from "react-redux";
import { closeSpinnerLoading, openSpinnerLoading } from "../actions/CommonAction";
import { saveSearchHistory, searchImageFromGalleryUsingKeywords } from "../actions/ImageGalleryAction";
import ImageGallery from "../components/main-components/ImageGallery";

const mapStateToProps = (state) => {
    const { imageGallery } = state;
    const { imageGalleryList, isSpinnerLoading, searchHistoryData } = imageGallery;
    return {
        imageGalleryList,
        isSpinnerLoading,
        searchHistoryData
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        searchImageFromGalleryUsingKeywords: async (keywords: String, callback: Function) => {
            await dispatch(searchImageFromGalleryUsingKeywords(keywords, callback));
        },
        openSpinnerLoading: () => {
            dispatch(openSpinnerLoading());
        },
        closeSpinnerLoading: () => {
            dispatch(closeSpinnerLoading());
        },
        saveSearchHistory: (searchHistoryData: Array) => {
            dispatch(saveSearchHistory(searchHistoryData));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageGallery)