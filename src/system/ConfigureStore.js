import storage from "redux-persist/lib/storage";
import { persistStore, persistCombineReducers } from "redux-persist";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { applyMiddleware, createStore } from "redux";
import RootReducer from '../reducers/RootReducer';


export const configureStore = () => {
    const persistConfig = {
        key: "root",
        storage,
        whilelist: [
            "imageGallery",
        ],
    };
    const persistedReducer = persistCombineReducers(persistConfig, RootReducer);
    const middleware = [thunk];
    middleware.push(logger);
    const store = createStore(persistedReducer, applyMiddleware(...middleware));
    const persistor = persistStore(store);
    return {
        store,
        persistor,
    };
};
