

export const styles = {
    searchBoxContainerStyle: {
        display: 'flex',
        justifyContent: 'center',
        margin: 'auto',
    },
    searchBoxStyle: {
        margin: '20px 0 20px',
        width: '100%',
        textAlign: 'center',
        fontSize: 14,
    },
    noImageAvailableStyle: {
        display: 'flex',
        margin: 'auto',
        paddingTop: '10%'
    },
    spinnerLoadingStyle: {
        display: 'flex',
        margin: 'auto',
    },
    searchBtnStyle: {
        margin: '20px 0 20px',
        height: 35,
        minWidth: '30%',
        textAlign: 'center',
        fontSize: 14,
    }
}