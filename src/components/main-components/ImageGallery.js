import React, {useState, useEffect} from "react";
import Gallery from "react-photo-gallery";
import { AutoComplete, Container, Button, Alert } from "rsuite";
import _ from 'lodash';
import { Ripple } from 'react-spinners-css';
import {styles} from "./styles/ImageGalleryStyle";
import noImageAvailable from '../../assets/icons/no-image-icon-15.png';
import { constructImageGalleryObj } from "../../actions/ImageGalleryAction";

const placeholderText = 'Type something...';
const searchBtn = 'Search';
const emptyKeyboardWarningText = 'Please, type something to search...!!!';

type Props = {
    isSpinnerLoading: Boolean,
    searchHistoryData: Array,
    imageGalleryList: Array<Object>,
    closeSpinnerLoading: Function,
    openSpinnerLoading: Function,
    saveSearchHistory: Function,
    searchImageFromGalleryUsingKeywords: Function,
}

const ImageGallery = (props: Props) => {
    const [keywords, setKeywords] = useState('');
    useEffect(() => {
        _searchImageFromGalleryUsingKeywords('');
    }, []);
    const {imageGalleryList, saveSearchHistory, searchImageFromGalleryUsingKeywords, searchHistoryData, openSpinnerLoading, closeSpinnerLoading, isSpinnerLoading} = props;
    const _searchImageFromGalleryUsingKeywords = async (value) => {
        openSpinnerLoading();
        await searchImageFromGalleryUsingKeywords(value, closeSpinnerLoading);
    }
    const _renderImageGalleryList = () => {
        const photos = constructImageGalleryObj(imageGalleryList);
        return  !_.isEmpty(photos) ? <Gallery photos={photos} lazyload={true} /> 
        : <img src={noImageAvailable} alt={noImageAvailable} style={noImageAvailableStyle} />;
    }
    const onSubmit = () => {
        const updatedSearchHistoryData = !_.includes(searchHistoryData, keywords) ? _.concat(searchHistoryData, keywords) : searchHistoryData;
        !_.isEmpty(keywords) ? _searchImageFromGalleryUsingKeywords(keywords) : Alert.warning(emptyKeyboardWarningText);
        saveSearchHistory(updatedSearchHistoryData);
    }
    const {searchBoxStyle, searchBtnStyle, searchBoxContainerStyle, noImageAvailableStyle, spinnerLoadingStyle} = styles;
    return (
        <Container>
            <div style={searchBoxContainerStyle}>
                <AutoComplete 
                    style={searchBoxStyle} 
                    placeholder={placeholderText}
                    data={searchHistoryData}
                    onChange={(value) => setKeywords(value)}
                />
                <Button color="cyan" style={searchBtnStyle} onClick={() => onSubmit()}>{searchBtn}</Button>
            </div>
            {!isSpinnerLoading ? _renderImageGalleryList() : <Ripple style={spinnerLoadingStyle} />}
        </Container>
    );
}

export default ImageGallery;