import { CLOSE_SPINNER_LOADING, OPEN_SPINNER_LOADING } from "../constants/ActionContants"

export const openSpinnerLoading = () => {
    return {
        type: OPEN_SPINNER_LOADING
    }
}

export const closeSpinnerLoading = () => {
    return {
        type: CLOSE_SPINNER_LOADING
    }
}