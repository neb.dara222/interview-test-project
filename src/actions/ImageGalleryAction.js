import {createApi} from "unsplash-js";
import _ from 'lodash';
import { SAVE_SEARCH_HISTORY, SEARCH_IMAGE_GALLERY } from "../constants/ActionContants";
import { ACCESS_KEY, DEFAULT_SEARCH_KEYWORD } from "../constants/CommonConstants";


const unsplashApi = new createApi({
    accessKey: ACCESS_KEY,
});

const doSearchImageFromGalleryUsingKeywords = (imageGallery: Array) => {
    return {
        type: SEARCH_IMAGE_GALLERY,
        payload: imageGallery
    }
}

export const searchImageFromGalleryUsingKeywords = (keywords: String, callback: Function) => {
    return async (dispatch: Function) => {
        try {
            const query = _.isEmpty(keywords) ? DEFAULT_SEARCH_KEYWORD : keywords;
            await unsplashApi.search
            .getPhotos({ query: query, orientation: "landscape"})
            .then(responses => {
                const results = _.get(responses, 'response.results', []);
                console.log('results', results);
                dispatch(doSearchImageFromGalleryUsingKeywords(results)); 
                callback && callback()
            }).catch(error => {
                 console.log(`Searching image from gallery error ${error}`);
            });
        } catch (error) {
            console.log(`Searching image from gallery error ${error}`);
        }
    }
}

export const constructImageGalleryObj = (imageGallery: Array) => {
    return _.map(imageGallery, (value) => {
        const width = _.get(value, 'width', 0);
        const height = _.get(value, 'height', 0);
        const src = _.get(value, 'urls.full', '');
        const thumbnail = _.get(value, 'urls.thumbnail', '');
        return _.assign({src, width, height, thumbnail});
    });
}

export const saveSearchHistory = (searchHistoryData: Array) => {
    return {
        type: SAVE_SEARCH_HISTORY,
        payload: searchHistoryData
    }
}