import ImageGalleryReducer from "./ImageGalleryReducer";

const RootReducer = {
    imageGallery: ImageGalleryReducer,
};

export default RootReducer;