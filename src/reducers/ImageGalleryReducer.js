import { CLOSE_SPINNER_LOADING, OPEN_SPINNER_LOADING, SAVE_SEARCH_HISTORY, SEARCH_IMAGE_GALLERY } from "../constants/ActionContants";

const INITIALIZED_STATE = {
    imageGalleryList: [],
    isSpinnerLoading: false,
    searchHistoryData: ['Developer']
};

const searchImageFromGalleryUsingKeywords = (state, action) => {
    return {
        ...state,
        imageGalleryList: action.payload
    };
};

const openSpinnerLoading = (state, action) => {
    return {
        ...state,
        isSpinnerLoading: true
    };
};

const closeSpinnerLoading = (state, action) => {
    return {
        ...state,
        isSpinnerLoading: false
    };
};

const saveSearchHistory  = (state, action) => {
    return {
        ...state,
        searchHistoryData: action.payload
    };
};

export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case SEARCH_IMAGE_GALLERY:
            return searchImageFromGalleryUsingKeywords(state, action);
        case OPEN_SPINNER_LOADING:
            return openSpinnerLoading(state);
        case CLOSE_SPINNER_LOADING:
            return closeSpinnerLoading(state);
        case SAVE_SEARCH_HISTORY: 
            return saveSearchHistory(state, action);
        default:
            return state;
    }
};